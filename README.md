# Udemy-frontend course PJ01
 A front end practicing project of the Udemy course. Coding with HTML5 and CSS.

這是我跟著線上學習課程做的第一個前端網站，除了使用基礎的HTML及CSS之外，也串接了Google Chart與Google Map。 
<br/>第一次做前端Project時比較覺得困難的部分是CSS的切版，在課程中老師有解釋Margin及Flex-Item的功能，但真正自己在做的時候必須要回去翻筆記才能記得如何使用這些語言，希望做下一個Project時已經可以熟悉流暢使用。

# Special Thanks 

Undemy course [2022網頁開發全攻略(HTML, CSS, JavaScript, React, SQL, Node, more)](https://www.udemy.com/course/html5-css3-z/)
<br/> Icon Using [Taiwan Icon Font ](https://www.twicon.page/)
